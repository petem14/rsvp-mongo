const express = require("express")
const bodyParser = require('body-parser')
const port = 3000
const app = express()

app.set('view engine', 'pug')
app.use(express.static('public'))
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }))

const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/rsvp');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
  // console.log(db)
});

//define schema
var responseSchema = new mongoose.Schema({
  name: String,
  email: String,
  attending: String,
  numberAttending: Number
});

//create model from schema
let Response = mongoose.model('Response', responseSchema)

app.get('/', (req, res) => {
  // console.log(Response.obj)
  res.render('index', { actionPath: '/reply' })
  // Response.deleteMany({},()=>console.log(Response))
})

app.post('/reply', (req, res) => {
  console.log(req.body)
  let newEntry = new Response({
    name: req.body.name,
    email: req.body.email,
    attending: req.body.attending,
    numberAttending: req.body.number
  })
  newEntry.save()

  res.render('reply',{})
})

app.get('/guests', (req, res) => {
  Response.find({}, (err, responses) => {
    console.log(responses)
    let attendArr = []
    let notAttendArr = []
    for (item of responses) {
      if (item.attending === 'yes') {
        attendArr.push(item)
      } else {
        notAttendArr.push(item)
      }
    }
    res.render('guests', { 
      attending: attendArr, 
      notAttending: notAttendArr 
    })
  })
  // console.log(currentDB)
  // res.send(currentDB)
})

app.listen(port)